package com.classpath.ordermicroservice.event;

import com.classpath.ordermicroservice.model.EventType;
import com.classpath.ordermicroservice.model.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@Data
@Getter
@RequiredArgsConstructor
public class OrderEvent {
    private final Order order;
    private final EventType eventType;
    private final LocalDateTime timestamp;
}
