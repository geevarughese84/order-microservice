package com.classpath.ordermicroservice.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
class DBHealthIndicator implements HealthIndicator{

    @Override
    public Health health() {
        //write the logic to check if the endpoint is healthy
        return Health.up().withDetail("DB", "DB is up").build();
    }
}
@Component
class GatewayHealthIndicator implements HealthIndicator{

    @Override
    public Health health() {
        //write the logic to check if the endpoint is healthy
        return Health.up().withDetail("Payment-Gateway", "Payment gateway is up").build();
    }
}
@Component
class KafkaHealthIndicator implements HealthIndicator{

    @Override
    public Health health() {
        //write the logic to check if the endpoint is healthy
        return Health.up().withDetail("Kafka-Endpoing", "Kafka endpoint is up").build();
    }
}
